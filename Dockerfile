FROM python:3.10.12

## ensure locale is set during build
ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8

ARG DEBIAN_FRONTEND=noninteractive

# Update all packages and install needed dependencies
RUN apt-get update && \
    apt-get install -y python3-pip && \
    apt-get install -y git && \
    apt-get install -y debconf-utils && \
    apt-get install -y h5utils && \
    echo "krb5-config krb5-config/add_servers_realm string CERN.CH" | debconf-set-selections && \
    echo "krb5-config krb5-config/default_realm string CERN.CH" | debconf-set-selections && \
    apt-get install -y krb5-user && \
    apt-get install -y nano vim emacs less screen graphviz python3-tk wget 

# Copy the requirements.txt for installation
COPY requirements.txt .

# Run pip upgrade and installation of python packages
RUN python -m pip install --upgrade pip
RUN python -m pip install ipython
RUN python -m pip install -r requirements.txt
